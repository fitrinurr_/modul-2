public class usingExceptions {
    private static final Exception Exception = null;
	    public static void main (String [] args){
        try{
            throwException();
        }catch(Exception exception){
            System.err.println("Esception handled in main");
        }
        doesNotThrowException();
    }
    public static void throwException() throws Exception {
        try{
            System.out.println("Method throwException");
            throw new Exception();
        } catch (Exception exception){
            System.err.println("Exception handled in method throwException");
            throw exception;
        } finally{
            System.err.println("Finally Executed in throwException");
        }
    }
    public static void doesNotThrowException(){
        try{
            System.out.println("Method doesNotThrowException");
        }catch (Exception exception){
            System.err.println("exception");
        }finally{
            System.err.println("Finally executed in doesNotThrowException");
        }
        System.err.println("End of method doesNotThrowException");
    }
}

