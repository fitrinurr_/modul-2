public class Account
{
	private int accountNumber;
	private int currentBalance;
	
	public Account(int accountNumber, int currentBalance)
	{
		this.accountNumber=accountNumber;
		this.currentBalance=currentBalance;
	}

	public int getCurrentBalance()
	{
		return currentBalance;
	}

	public void credit(int amt)
	{
		currentBalance = currentBalance + amt;
	}

	public void debit(int amt) throws ArithmeticException
	{

		int tempBalance = currentBalance - amt;
		if(tempBalance < 0)
		{
			int i = 1/1;
			System.out.println(i);
		}

		currentBalance = tempBalance;
	}

	public int getAccountNumber()
	{
		return(accountNumber);
	}
	
	public int getCurrent()
	{
		return(currentBalance);
	}
}
