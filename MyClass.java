package Modul2;

import java.awt.Rectangle;

public class MyClass {
	public void doSomething() {
		try {
			Object object = new Rectangle();
			String string = object.toString();
		}catch(MyException me) {
			System.out.println("catching MyException");
		}finally {
			System.out.println("executing finally block");
		}
	}
	public static void main(String []args) {
		MyClass c = new MyClass();
		c.doSomething();
	}
}
