
import java.util.Scanner;

public class DivideByZeroNoExceptionHandling {
    public static int pembagian(int bil, int pbg){
        return bil/pbg;
    }
    
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Masukan Nilai Pembilang : ");
        int bil = scanner.nextInt();
        System.out.print("Masukan NilaiPenyebut (pembagi) : ");
        int pbg = scanner.nextInt();
        int result = pembagian(bil, pbg);
        System.out.printf("%nResult: %d / %d = %d%n", +bil, pbg, result);
    }
}
