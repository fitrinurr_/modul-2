import java.util.InputMismatchException;
import java.util.Scanner;

public class DivideByZeroWithExceptionhandling {
	public static int pembagian (int bil,int pbg) throws ArithmeticException{
		return bil/pbg;
	}
	public static void main (String [] args) {
		Scanner scanner = new Scanner (System.in);
		boolean continueLoop = true;
		do {
			try {
				System.out.print("please enter an integer numberatir: ");
				int numerator = scanner.nextInt();
				System.out.print("please enter an integer denominator :");
				char denominator = (char) scanner.nextInt();
				float result = pembagian(numerator, denominator);
				System.out.print("result :" +result);
				continueLoop = false;
			}
			catch (InputMismatchException e) {
				String inputMismatchException = null;
				System.err.print("Exception =" +inputMismatchException);
				scanner.nextLine();
				System.out.print("you must enter integers. please try again!");
			}
			catch (ArithmeticException aritmeticException) {
				String arithmeticException = null;
				System.err.print("exception :" +arithmeticException);
				System.out.print("zero is an invalid denominator. please try again!!");
			}
		}	while (continueLoop);
	}
}